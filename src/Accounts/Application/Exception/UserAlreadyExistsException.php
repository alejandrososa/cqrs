<?php
/**
 * Created by PhpStorm.
 * User: alejandro
 * Date: 27/12/17
 * Time: 15:51
 */

namespace App\Accounts\Application\Exception;

/**
 * Class UserAlreadyExistsException
 * @package App\Domain\Exception\User
 */
class UserAlreadyExistsException extends \Exception
{

}