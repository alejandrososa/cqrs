<?php
/**
 * Created by PhpStorm.
 * User: grace
 * Date: 26/12/17
 * Time: 16:03
 */

namespace App\Accounts\Domain\Model\User;

use App\Common\Application\Event\DomainEventPublisher;
use App\Common\Domain\Model\Event\AggregateRoot;

/**
 * Class User
 * @package App\Accounts\Domain\Model\User
 */
class User extends AggregateRoot
{
    /**
     * @var UserId
     */
    private $id;

    /**
     * @var UserName
     */
    private $name;

    /**
     * @var UserEmail
     */
    private $email;

    /**
     * @var UserStatus
     */
    private $status;

    /**
     * User constructor.
     * @param UserId $id
     * @param UserName $name
     * @param UserEmail $email
     * @param UserStatus $status
     */
    public function __construct(UserId $id, UserName $name, UserEmail $email, UserStatus $status)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->status = $status;
        $this->create();
    }

    /**
     * @return UserId
     */
    public function id(): UserId
    {
        return $this->id;
    }

    /**
     * @return UserName
     */
    public function name(): UserName
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function email(): string
    {
        return $this->email;
    }

    /**
     * @return UserStatus
     */
    public function status(): UserStatus
    {
        return $this->status;
    }

    protected function create()
    {
        $this->recordApplyAndPublishThat(
            new UserRegistered($this->id, $this->name, $this->email, $this->status)
        );
    }

    protected function changeStatus(UserStatus $userStatus)
    {
        $this->recordApplyAndPublishThat(
            new UserStatusChanged($this->id, $userStatus)
        );
    }

    protected function applyUserRegistered(UserRegistered $event)
    {
        $this->id = $event->userId();
        $this->name = $event->userName();
        $this->email = $event->userEmail();
        $this->status = $event->userStatus();
    }

//    protected function applyUserStatusChanged(UserRegistered $event)
//    {
//        $this->id = $event->userId();
//        $this->name = $event->userName();
//        $this->email = $event->userEmail();
//        $this->status = $event->userStatus();
//    }
}