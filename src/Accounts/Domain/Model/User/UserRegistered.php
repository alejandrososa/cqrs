<?php
/**
 * Created by PhpStorm.
 * User: alejandro
 * Date: 5/01/18
 * Time: 11:03
 */

namespace App\Accounts\Domain\Model\User;

use App\Common\Domain\Model\Event\DomainEvent;

/**
 * Class UserRegistered
 * @package App\Accounts\Domain\Model\User
 */
class UserRegistered implements DomainEvent
{
    private $userId;
    private $ocurredOn;
    /**
     * @var UserName
     */
    private $userName;
    /**
     * @var UserEmail
     */
    private $userEmail;
    /**
     * @var UserStatus
     */
    private $userStatus;

    /**
     * UserRegistered constructor.
     * @param UserId $userId
     */
    public function __construct(UserId $userId, UserName $userName, UserEmail $userEmail, UserStatus $userStatus)
    {
        $this->userId = $userId;
        $this->userName = $userName;
        $this->userEmail = $userEmail;
        $this->userStatus = $userStatus;
        $this->ocurredOn = new \DateTimeImmutable();
    }

    /**
     * @return \DateTimeImmutable
     */
    public function occurredOn(): \DateTimeImmutable
    {
        return $this->ocurredOn;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return UserName
     */
    public function userName(): UserName
    {
        return $this->userName;
    }

    /**
     * @return UserEmail
     */
    public function userEmail(): UserEmail
    {
        return $this->userEmail;
    }

    /**
     * @return UserStatus
     */
    public function userStatus(): UserStatus
    {
        return $this->userStatus;
    }
}