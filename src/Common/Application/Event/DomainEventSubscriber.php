<?php
/**
 * Created by PhpStorm.
 * User: alejandro
 * Date: 2/01/18
 * Time: 17:56
 */

namespace App\Common\Application\Event;

use App\Common\Domain\Model\Event\DomainEvent;

/**
 * Interface DomainEventSubscriber
 * @package App\Common\Application\Event
 */
interface DomainEventSubscriber
{
    /**
     * @param DomainEvent $aDomainEvent
     */
    public function handle($aDomainEvent);

    /**
     * @param DomainEvent $aDomainEvent
     * @return bool
     */
    public function isSubscribedTo($aDomainEvent);
}