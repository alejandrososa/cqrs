Visión de conjunto
==
Entonces, ¿qué es exactamente un Command Bus?

Patrón de comando

El rol del Command Bus es asegurar el transporte de un Comando a su Handler. El Bus de Comandos recibe un Comando, que no es más que un mensaje que describe la intención, y lo pasa a un Manejador que luego es responsable de realizar el comportamiento esperado. Por lo tanto, este proceso se puede considerar como una llamada a la Capa de Servicio, donde el Bus de Comando realiza la canalización del mensaje en el medio.

Antes de la introducción del Command Bus, Service Layers a menudo era una colección de clases sin una forma estándar de invocación. Los buses de comando resuelven este problema al proporcionar una interfaz consistente y definir mejor el límite entre dos capas. La interfaz estándar también permite agregar funcionalidades adicionales envolviendo decoradores o agregando middleware.

Por lo tanto, puede haber más en un Command Bus que llamando a una Capa de servicio. Las implementaciones básicas solo pueden ubicar un Controlador basado en convenciones de nomenclatura, pero las configuraciones más complejas pueden pasar un Comando a través de una interconexión. Los ductos pueden realizar un comportamiento adicional, como: ajustar el comportamiento en una transacción; enviando el comando a través de una red; o tal vez algunas colas y registros.

Antes de analizar de cerca las ventajas de usar un Command Bus, echemos un vistazo a los componentes individuales que lo hacen posible.

Comandos
--

Para complicar un poco las cosas, el patrón ha evolucionado con diseños alternativos. Los patrones arquitectónicos como CQS (Separación de consulta de comandos) y CQRS (Segregación de responsabilidad de consulta de comandos) también usan comandos, sin embargo, en su contexto, un comando es simplemente un mensaje.

Tradicionalmente, un comando GoF se manejaría solo:


    final class CreateDeck
    {
        /**
           * @var string
         */
        private $id;
    
        /**
           * @param string $id
         */
        public function __construct($id)
        {
            $this->id = $id;
        }
    
        /**
           * @return DeckId
         */
        public function getId()
        {
            return DeckId::fromString($this->id);
        }
    }

Los comandos se conocen comúnmente como DTO (objetos de transferencia de datos) ya que se utilizan para contener datos que se transportan de una ubicación a otra. Los comandos son por lo tanto inmutables. Después de la creación, no se espera que los datos cambien. Notarás que nuestro CreateDeckcomando de ejemplo no contiene setters, o cualquier otra forma de alterar el estado interno. Esto asegura que no puede cambiar durante el tránsito al controlador.


Manejadores de comandos
--

Los manejadores interpretan la intención de un Comando específico y realizan el comportamiento esperado. Tienen una relación 1: 1 con los comandos, lo que significa que para cada comando, solo hay un solo controlador.

    final class CreateDeckHandler
    {
        /**
           * @var DeckRepository
         */
        private $decks;
    
        /**
           * @param DeckRepository $decks
         */
        public function __construct(DeckRepository $decks)
        {
            $this->decks = $decks;
        }
    
        /**
           * @param CreateDeck $command
         */
        public function handle(CreateDeck $command)
        {
            $id = $command->getId();
    
            $deck = Deck::standard($id);
    
            $this->decks->add($deck);
        }
    }


En nuestro ejemplo anterior, se está creando un nuevo Deck. Lo que también es importante tener en cuenta es lo que no está sucediendo. No llena una vista; devuelve un código de respuesta HTTP o escribe en la consola. Los comandos se pueden ejecutar desde cualquier lugar, por lo tanto, los manejadores siguen siendo agnósticos para el entorno de llamadas. Esto es extremadamente poderoso al diseñar límites entre su aplicación y el mundo exterior.

El bus de comando
--

Y finalmente, el Bus de Comandos mismo. Como se explicó brevemente anteriormente, la responsabilidad de un Command Bus es pasar un Command en su Handler. Veamos un ejemplo.

Imagine que necesitábamos exponer un punto final RESTful API para permitir la creación de nuevos Decks:

    final class DeckApiController
    {
        /**
           * @var CommandBus
         */
        private $bus;
    
        /**
           * @var CommandBus $bus
         */
        public function __construct(CommandBus $bus)
        {
            $this->bus = $bus;
        }
    
        /**
           * @var Request $request
         */
        public function create(Request $request)
        {
            $deckId = $request->input('id');
    
            $this->bus->execute(
                new CreateDeck($deckId)
            );
    
            return Response::make("", 202);
        }
    }

https://www.sitepoint.com/command-buses-demystified-a-look-at-the-tactician-package/

https://zawarstwaabstrakcji.pl/20170130-cqrs-w-praktyce-wprowadzenie-php/